# EEE3088F PiHat Project

This is a UPS system for a microcontroller.
It should supply backup power incase of any power shortages or surges.
This back up power kicks in at critical times to ensure that data on computers is not lost due to sudden power cuts etc.

It should be portable and simple to use
It must be affordable and convenient
It must be asthetically pleasing.
